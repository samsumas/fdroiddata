Categories:Multimedia
License:GPL-3.0-only
Author Name:Ivan D'Ortenzio
Web Site:
Source Code:https://github.com/enricocid/Music-Player-GO
Issue Tracker:https://github.com/enricocid/Music-Player-GO/issues
Changelog:https://github.com/enricocid/Music-Player-GO/releases

Auto Name:Music Player GO
Summary:Very slim music player
Description:
''Music Player GO'' is a very slim mucic player (app size only ~1M). It offers a
colorful and simple "Unified UI" (no need to navigate between activities). The
app also features an Equalizer, Themes (Light, dark, night) andmore. It even
deals with Pause/resume when the headsets are (dis)connected.
.

Repo Type:git
Repo:https://github.com/enricocid/Music-Player-GO.git

Build:2.3.3.3,49
    commit=v2.3.3.3
    subdir=project/app
    gradle=yes

Build:2.4.1.5,56
    commit=v2.4.1.5
    subdir=project/app
    gradle=yes

Build:2.4.1.8,59
    commit=v2.4.1.8
    subdir=project/app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.4.1.8
Current Version Code:59
